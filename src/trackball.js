
var mat4 = require('gl-mat4');

module.exports = function Trackball(opts) {
    
    var self = this;
    
    var speed = opts.speed || 0.005;
    var onRotate = opts.onRotate || function() {};

    self.rotation = mat4.create();

    self.rotate = function(dx, dy) {
        var rot = mat4.create();
        mat4.rotateY(rot, rot, dx * speed);
        mat4.rotateX(rot, rot, dy * speed);
        mat4.multiply(self.rotation, rot, self.rotation);
        onRotate();
    }
}
